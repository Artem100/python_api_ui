# Python_API_UI

## General info
Project for creation UI and API tests

## Technologies
* Python 3.7;
* Page Object;
* JSON parser;
* Selenium;
* Requests;
* Pytest;
* Allure;
* Bat/Shell.

## Setup
To run this project: <br />
0.Install **Python 3**, **JDK 1.8** and **Allure** plugin on your PC or Server.  <br />
1.Create the folder in **C** disk for local allure reports by path: *C:/AllureReports/*<br />
2.Run command in command to setup the libraries and tools of project:
```
pip install -r requirements.txt
```
3.To run tests in project with generation of allure report use executable **bat** files in project:
* AllTests - Run all tests in project.
* ByMark - Run tests marker name (pytest.mark).
* RunSuite - Run tests my name of module (suite).
* UniqueTestChrome - before running tests, you will enter name or ID of test case. All tests will run in Chrome browser.
* UniqueTestAndBrowser - before running tests, you will enter name of browser and ID of test case.
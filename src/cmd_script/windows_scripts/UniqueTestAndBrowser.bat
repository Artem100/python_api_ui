set /P path_project="Path to automation project: "
set /P tests="What tests do you want?: "
set /P browser="What browser do you want?: "
set datetemp=%Date:~10,4%_%Date:~7,2%_%Date:~4,2%
pytest %path_project% --log-format="%%(message)s" -k "%tests%" --browser %browser% --color=no --alluredir=C:/AllureReports/%datetemp%/reports/allure-results
allure serve C:/AllureReports/%datetemp%/reports/allure-results
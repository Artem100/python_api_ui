set /P path_project="Path to automation project: "
set marks=home or userprofile or companyProfile or Dashboard or ContractTab or ContractDashboard or DocumentaryInstructions
set dayMonthYear=%Date:~7,2%_%Date:~4,2%_%Date:~10,4%
set datetemp=%Date:~7,2%_%Date:~4,2%_%Date:~10,4%_%time:~0,2%_%time:~3,2%
set browser=edge
pytest %path_project% --log-format="%%(message)s" -m "%marks%" --browser %browser% --reruns 3 --color=no --alluredir=C:/AllureReports/%dayMonthYear%/Edge_UI/%datetemp%/reports/allure-results
allure serve C:/AllureReports/%dayMonthYear%/Edge_UI/%datetemp%/reports/allure-results
set /P path_project="Path to automation project: "
set /P suites="What suites do you want?: "
set datetemp=%Date:~10,4%_%Date:~7,2%_%Date:~4,2%
pytest %path_project% --log-format="%%(message)s" -k "test_%suites%".py --color=no --alluredir=C:/AllureReports/%datetemp%/reports/allure-results
allure serve C:/AllureReports/%datetemp%/reports/allure-results
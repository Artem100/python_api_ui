import allure
from selenium.webdriver.common.by import By

from src.resources.data_params import DataUI
from src.ui.page_object.base_page import BasePage


class UserActions(BasePage):

    # TITLES, USER, USER BAR LOCATORS AND ACTION WITH THEM

    # URLS
    URL_LOGIN_PAGE = DataUI().MAIN_PAGE_UI_URL
    # TITLES
    TITLE_LOGIN_PAGE = "Login page"
    TITLE_MY_ACCOUNT = "My account"
    # SELECTORS
    SUBMIT_BUTTON = (By.CSS_SELECTOR, "button.submit", "SUBMIT BUTTON")

    @allure.step("At *LOGIN* page")
    def at_login_page(self):
        self.title_page(self.TITLE_LOGIN_PAGE)
        return self

    @allure.step("Open *LOGIN* page")
    def open_login_page_via_url(self):
        self.check_url_of_page_doesnt_match(self.URL_LOGIN_PAGE)
        self.open(self.URL_LOGIN_PAGE)
        self.at_login_page()
        return self


    @allure.step("At *ACCOUNT* page")
    def at_my_account_page(self):
        self.title_page(self.TITLE_MY_ACCOUNT)
        return self

    @allure.step("Click *Submit* button")
    def submit_button_click(self):
        self.click(*self.SUBMIT_BUTTON)
        return self


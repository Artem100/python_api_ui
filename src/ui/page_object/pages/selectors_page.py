from selenium.webdriver.common.by import By

from src.ui.page_object.pages.user_actions import UserActions
from src.resources.data_params import DataUI


class LoginPage(UserActions):

    # Example by login page

    URL_LOGIN_PAGE = DataUI().MAIN_PAGE_UI_URL
    EMAIL_FIELD = (By.CSS_SELECTOR, "email", "EMAIL FIELD")
    EMAIL_FIELD_ERROR = (By.CSS_SELECTOR, "email.error", "EMAIL FIELD ERROR")
    PASSWORD_FIELD = (By.CSS_SELECTOR, "password", "PASSWORD FIELD")
    PASSWORD_FIELD_ERROR = (By.CSS_SELECTOR, "password.error", "PASSWORD FIELD ERROR")
    SUBMIT_BUTTON = (By.CSS_SELECTOR, "button.submit", "SUBMIT BUTTON")
    ERROR_LOGIN = (By.CSS_SELECTOR, "login-error", "ERROR LOGIN")

    def at_page(self):
        self.title_page(self.TITLE_LOGIN_PAGE)

    def open_login_page_via_url(self):
        self.check_url_of_page_doesnt_match(self.URL_LOGIN_PAGE)
        self.open(self.URL_LOGIN_PAGE)
        return self

    def email_field_input(self, user):
        self.edit_text_field(user, *self.EMAIL_FIELD)

    def password_field_input(self, password):
        self.edit_text_field(password, *self.PASSWORD_FIELD)
        return self

    def error_login_check_text(self, value):
        self.check_text_in_element(value, *self.ERROR_LOGIN)
        return self

    def email_field_error(self):
        self.displayed_element(*self.EMAIL_FIELD_ERROR)
        return self

    def password_field_error(self):
        self.displayed_element(*self.PASSWORD_FIELD_ERROR)
        return self

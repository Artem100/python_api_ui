import allure

from src.resources.data_params import DataUI
from src.ui.page_object.pages.selectors_page import LoginPage


class LoginSteps(LoginPage):

    @allure.step("Open *Login* page and login to account")
    def open_login_to_account(self, user):
        self.open_login_page_via_url()
        self.email_field_input(DataUI().email(user))
        self.password_field_input(DataUI().password(user))
        self.submit_button_click()
        self.at_my_account_page()
        return self

    @allure.step("Check *Login error* with invalid data")
    def error_in_login_page(self, value):
        self.error_login_check_text(value)
        self.email_field_error()
        self.password_field_error()
        self.at_login_page()
        return self
import datetime
import logging
import os
import shutil
import time
from random import choice

from faker import Faker
from selenium.common.exceptions import TimeoutException, NoSuchElementException, JavascriptException, \
	WebDriverException, InvalidArgumentException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.errorhandler import ErrorHandler
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait


def format_selector(by, locator, name=None):
	selector = (by, locator)
	name = name if name else selector
	return selector, name


class BasePage(object):
	def __init__(self, driver):
		self.driver = driver

	LOGGER = logging.getLogger(__name__)
	faker = Faker()

	driver = None
	timeout = 10

	LOADING_ICON = (By.CSS_SELECTOR, "span.loader", "LOADING ICON")


	def timeout_element_error(self, selector, name):
		""" Timeout Webdriver and NoSuchElementException"""
		BasePage.LOGGER.error("Timeout - < {1} > element not found: {0} \n".format(selector, name))
		raise Exception("Timeout - < {1} > element not found: {0}".format(selector, name))

	# AssertionError
	def element_doesnt_contain_expected_value_error(self, name, value, result):
		BasePage.LOGGER.error("Element < {0} > doesn't contain expected value.\nExpected value: {1}\nActual value: {2}".format(name, value, result))
		raise AssertionError("Element < {0} > doesn't contain expected value. \nExpected value: {1},\nActual value: {2}".format(name, value, result))

	def cant_click_on_the_element_error(self, selector, name):
		"""ErrorHandler, ElementClickInterceptedException"""
		BasePage.LOGGER.error("Can't click on {0} element: {1}".format(name, selector))
		assert Exception("Can't click on {0} element: {1}".format(name, selector))

	# WebDriverWait
	def element_isnt_display_on_page(self, selector, name):
		BasePage.LOGGER.error(
			"WebDriverWait exception: Element < {1} > isn't displayed: {0} on page".format(selector, name))
		assert Exception("WebDriverWait exception: Element < {1} > isn't displayed: {0} on page".format(selector, name))

	def open(self, url):
		BasePage.LOGGER.info("Open page by url < {} >".format(url))
		self.driver.get(url)

	def close(self):
		self.driver.close()

	def delete_cookies(self):
		self.driver.delete_all_cookies()

	# UI elements like titles or icons
	def find_element_located(self, selector, name):
		try:
			element = WebDriverWait(self.driver, BasePage.timeout).until(EC.presence_of_element_located(selector))
			return element
		except (TimeoutException, NoSuchElementException):
			self.timeout_element_error(selector, name)
		except ErrorHandler:
			self.cant_click_on_the_element_error(selector, name)

	# UI elements like buttons
	def find_element_clickable(self, selector, name):
		try:
			element = WebDriverWait(self.driver, BasePage.timeout).until(EC.element_to_be_clickable(selector))
			return element
		except (TimeoutException, NoSuchElementException):
			self.timeout_element_error(selector, name)
		except ErrorHandler:
			self.cant_click_on_the_element_error(selector, name)

	def find_elements_visibility(self, selector, name):
		self.LOGGER.info("Find elements < {} > ".format(name))
		try:
			elements = WebDriverWait(self.driver, BasePage.timeout).until(EC.visibility_of_all_elements_located(selector))
			return elements
		except (TimeoutException, NoSuchElementException):
			self.timeout_element_error(selector, name)
		except ErrorHandler:
			self.cant_click_on_the_element_error(selector, name)


	def iframe_element_located(self, selector, name):
		self.LOGGER.info("Switch iframe < {} > on page".format(name))
		try:
			iframe = (WebDriverWait(self.driver, BasePage.timeout)).until(
				EC.frame_to_be_available_and_switch_to_it(selector))
			return iframe
		except (TimeoutException, NoSuchElementException):
			self.timeout_element_error(selector, name)
		except ErrorHandler:
			self.cant_click_on_the_element_error(selector, name)



	def click(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Click on < {} >".format(name))
		try:
			element.click()
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't click on element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't click on element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
		# except ErrorHandler:
		# 	element.click()

	def enter_text(self, key_send, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Input text value:* {} * to < {} > element.".format(key_send, name))
		try:
			element.send_keys(key_send)
		except Exception as e:
			BasePage.LOGGER.warning("Test input text to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't input text to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))

	def upload_image(self, path, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Upload image by path : * {} * to < {} > element.".format(path, name))
		try:
			element.send_keys(path)
		except Exception as e:
			BasePage.LOGGER.warning("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))

	def upload_file(self, path, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Upload file by path : * {} * to < {} > element.".format(path, name))
		try:
			element.send_keys(path)
		except InvalidArgumentException as e:
			BasePage.LOGGER.warning(f"Test can't file in project by path: < {path} >\n{e}")
			raise Exception(f"Test can't file in project by path: < {path} >\n{e}")
		except Exception as e:
			BasePage.LOGGER.warning("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))

	def wait_when_element_is_disappeared(self, *selector):
		# until_not - ожидаем что элемент исчезает
		selector, name = format_selector(*selector)
		self.LOGGER.info("Wait when < {} > will be missing".format(name))
		try:
			element = WebDriverWait(self.driver, 20).until(EC.invisibility_of_element_located(selector))
			return element
		except Exception:
			BasePage.LOGGER.error("Element < {0} > isn't invisible: {1} ".format(name, selector))
			raise Exception("Element < {0} > isn't invisible: {1}".format(name, selector))
		return True

	def check_doesnt_element_missing(self, *selector):
		"""
		Check that element mustn't display on page.


		:param selector:
		:return:
		"""
		selector, name = format_selector(*selector)
		self.LOGGER.info("Element when < {} > doesn't display".format(name))
		try:
			element = WebDriverWait(self.driver, 2).until(EC.invisibility_of_element_located(selector))
			return True
		except Exception:
			BasePage.LOGGER.error("Element < {0} > isn't invisible: {1} ".format(name, selector))
			raise Exception("Element < {0} > isn't invisible: {1}".format(name, selector))

	def check_text_in_element(self, text, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Check value: *{}* in < {} > element".format(text, name))
		try:
			result = element.text
			assert text == result
		except AssertionError:
			self.element_doesnt_contain_expected_value_error(name, text, result)

	def check_text_in_icons_or_messages(self, text, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info(f"Check text {text}* in {name} icon (message, history)")
		try:
			result = element.text
			assert text == result
		except AssertionError:
			BasePage.LOGGER.warning(f"Icon (message) < {name} > doen't contain expected text."
									f"\nExpected text: {text}\nActual text: {result} ")

	def check_text_in_list_elements(self, text, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		self.LOGGER.info("Check value: *{}* in < {} > element".format(text, name))
		value_list = []
		elements.text
		print("\n\n\n"+elements.text+"\n\n\n")
		# for i in value_list:
		# 	if text in i:
		# 		continue
		# 	else:
		# 		raise AssertionError (self.element_doesnt_contain_expected_value_error(name, text, i))

	def get_text_value_from_element(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Get text value from element < {} >".format(name))
		try:
			result = element.text
			self.LOGGER.info(f"Text value is: *{result}*")
			return result
		except Exception:
			BasePage.LOGGER.error("Test cant get value from < {0} > element: \n{1} ".format(name, selector))
			raise Exception("Test cant get value from < {0} > element: \n{1} ".format(name, selector))

	def check_url_of_page(self, expected_url):
		self.LOGGER.info("Check URL of < {0} > page".format(expected_url))
		try:
			assert expected_url in self.driver.current_url
		except AssertionError:
			BasePage.LOGGER.error("\nYou ARE NOT on expected page: {0}, \nYou are on the page: {1}".format(expected_url,
																										   self.driver.current_url))
			raise AssertionError("\nYou ARE NOT on expected page: {0}, \nYou are on the page: {1}".format(expected_url,
																										  self.driver.current_url))

	def check_url_of_page_doesnt_match(self, expected_url):
		self.LOGGER.info("Check URL of page doesn't match with expected URL")
		try:
			if self.driver.current_url != expected_url:
				if self.driver.current_url == "data:," or None:
					return True
				else:
					self.delete_all_cookies_in_local_storage()
					return True
		except AssertionError:
			BasePage.LOGGER.error(
				"Current URL < {0} > MUST NOT match with expected URL \n< {1} >".format(self.driver.current_url,
																						expected_url))
			raise AssertionError(
				"Current URL < {0} > MUST NOT match with expected URL \n< {1} >".format(self.driver.current_url,
																						expected_url))

	def select_element_from_the_list(self, number, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		self.LOGGER.info("Select element < {} > from the list by index *{}*".format(name, number))
		try:
			elements[number].click()
		except Exception as e:
			BasePage.LOGGER.warning("Test couldn't select element < {0} > by index *{1}* in the list: {2}\n{3}".format(name, number, selector, e))
			raise Exception("Test couldn't select element < {0} >  by index *{1}* in the list: {2}\n{3}".format(name, number,selector, e))

	def ctr_v_combination(self, path, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Past value to field : * {} * to < {} > element.".format(path, name))
		try:
			element.send_keys(Keys.CONTROL+Keys.V)
		except Exception as e:
			BasePage.LOGGER.warning("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't upload image to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))

	def displayed_element(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Check that element < {} > is displayed on page".format(name))
		try:
			return element
		except AssertionError as e:
			BasePage.LOGGER.error("Element < {0} > is displayed on page. {1} \n{2}}".format(name, selector, e))
			raise AssertionError("Element < {0} > is displayed on page. {1} \n{2}}".format(name, selector, e))


	def scroll_down_by_element(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Scroll down by element < {} >".format(name))
		try:
			self.driver.execute_script("arguments[0].scrollIntoView();", element)
		except Exception as e:
			BasePage.LOGGER.warning("Test can't scroll  by element < {0} > {1} \n{2}}".format(name, selector, e))
			raise Exception("Test can't scroll  by element < {0} > {1} \n{2}}".format(name, selector, e))

	def scroll_top_page(self):
		try:
			ActionChains(self.driver).key_down(Keys.HOME).perform()
			self.LOGGER.info("Scroll by top page")
		except Exception as e:
			BasePage.LOGGER.warning("Test can't scroll by top page")
			raise Exception("Test can't scroll by top page")


	def scroll_down(self):
		try:
			ActionChains(self.driver).key_down(Keys.END).perform()
			self.LOGGER.info("Scroll by end page")
		except Exception as e:
			BasePage.LOGGER.warning("Test can't scroll by end page")
			raise Exception("Test can't scroll by end page")

	def delete_all_cookies_in_local_storage(self):
		try:
			self.driver.execute_script("localStorage.removeItem('some:access_token');")
		except (JavascriptException, WebDriverException):
			return True

	def clear_text_field(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Clear text field < {} >".format(name))
		try:
			element.clear()
		except Exception as e:
			BasePage.LOGGER.warning("Test can't clear text in < {0} > field. {1} \n{2}}".format(name, selector, e))
			raise Exception("Test can't clear text in < {0} > field. {1} \n{2}}".format(name, selector, e))

	######
	def backspace_button_all(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Clear text field < {} > use the BASCKSPACE button".format(name))
		try:
			length = len(element.get_attribute('value'))
			element.send_keys(length * Keys.BACKSPACE)
		except TypeError:
			BasePage.LOGGER.warning("Field is clear already")
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't clear text in < {0} > field use the BASCKSPACE button. {1} \n{2}".format(name, selector, e))
			raise Exception(
				"Test can't clear text in < {0} > field use the BASCKSPACE button. {1} \n{2}".format(name, selector, e))

	def backspace_button(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Clear text field < {} > use the BASCKSPACE button".format(name))
		try:
			element.send_keys(Keys.BACKSPACE)
		except TypeError:
			BasePage.LOGGER.warning("Field is clear already")
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't clear text in < {0} > field use the BASCKSPACE button. {1} \n{2}".format(name, selector, e))
			raise Exception(
				"Test can't clear text in < {0} > field use the BASCKSPACE button. {1} \n{2}".format(name, selector, e))

	def count_of_elements(self, count, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		self.LOGGER.info("Check count of < {} > elements".format(name))
		try:
			assert len(elements) == int(count)
		except AssertionError:
			BasePage.LOGGER.error(
				"Count value doesn't match with test in element < {0} >.\nActual result: {1}\nExpected result: {2}".format(
					name, len(elements), count))
			raise AssertionError(
				"Count value doesn't match with test in element < {0} >.\nActual result: {1}\nExpected result: {2}".format(
					name, len(elements), count))


	def title_page(self, page_title):
		current_page = self.driver.title
		self.wait_when_element_is_disappeared(*self.LOADING_ICON)
		try:
			self.LOGGER.info("Check TITLE of page < {} > ".format(page_title))
			current_page = WebDriverWait(self.driver, BasePage.timeout).until(EC.title_contains(page_title))
			self.LOGGER.info("------< {} > page------".format(page_title))
		except AssertionError:
			BasePage.LOGGER.error(f"\nCurrent title of page doesn't compare with expected title.\nExpected title: {page_title}\nActual title: {current_page}")
			raise AssertionError(f"\nCurrent title of page doesn't compare with expected title.\nExpected title: {page_title}\nActual title: {current_page}")
		except TimeoutException:
			BasePage.LOGGER.error(f"\nCurrent title of page doesn't compare with expected title.\nExpected title: {page_title}\nActual title: {current_page}")
			raise AssertionError(f"\nCurrent title of page doesn't compare with expected title.\nExpected title: {page_title}\nActual title: {current_page}")

	def compare_variable_url_with_current(self, value_url):
		self.LOGGER.info("URL value must * {} * display in browser tab".format(value_url))
		tab_url = self.driver.current_url
		try:
			assert tab_url == value_url
		except AssertionError:
			BasePage.LOGGER.error(
				"Element doesn't contain expected url. \nExpected value: {0},\n  Actual value: {1}".format(value_url, tab_url))
			raise AssertionError(
				"Element doesn't contain expected url. \nExpected value: {0},\n  Actual value: {1}".format(value_url, tab_url))

	def get_attribute_from_element(self, attribute_name, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Get value from <{0}> attribute at element < {1} > ".format(attribute_name, name))
		try:
			value = element.get_attribute(attribute_name)
			self.LOGGER.info(f"Get value: *{value}*")
			return value
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't get value from < {0} > attribute at element < {1} >. {2}\n{3}".format(attribute_name, name,
																								  selector, e))
			raise Exception(
				"Test can't get value from < {0} > attribute at element < {1} >. {2}\n{3}".format(attribute_name, name,
																								  selector, e))

	def check_attribute_value_in_element(self, text, attribute_name, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Check value * {} * is present in attribute < {} >  of element < {} >".format(text, attribute_name, name))
		try:
			result = element.get_attribute(attribute_name)
			assert text == result
		except TimeoutException:
			self.timeout_element_error(selector, name)
		except AssertionError:
			self.element_doesnt_contain_expected_value_error(name, text, result)


	def return_text_value_from_attribute_value(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Return text value from attribute value in element < {} > ".format(name))
		try:
			result = element.get_attribute("value")
			return result
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't return text value from attribute 'value' in  < {0} > element. {1}\n{2}".format(name,
																										   selector, e))
			raise Exception(
				"Test can't return text value from attribute 'value' in  < {0} > element. {1}\n{2}".format(name,
																										   selector, e))

	def text_field_is_empty(self, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_located(selector, name)
		self.LOGGER.info("Text field must be empty in element < {} > ".format(name))
		try:
			length = len(element.get_attribute('value'))
			assert length == 0
		except AssertionError:
			BasePage.LOGGER.error("The text < {0} > field isn't empty. \n{1}".format(name, selector))
			raise AssertionError("The text < {0} > field isn't empty. \n{1}".format(name, selector))

	def read_values_from_elements(self, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		self.LOGGER.info("Read values from elements < {} > ".format(name))
		try:
			values = []
			for value in elements:
				values.append(value.get_attribute("value"))
			return values
		except Exception as e:
			BasePage.LOGGER.warning("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))


	def read_values_from_elements_and_make_list_text_values(self, *selector):
		""""
		Create list from text values of list elements, than check text_value on occurrence in list
		"""""
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		try:
			self.LOGGER.info("Read values from elements < {} > and save values in list".format(name))
			values = []
			for value in elements:
				values.append(value.text)
			return values
		except Exception as e:
			BasePage.LOGGER.warning("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))

	def read_text_from_elements_and_check_first_text_value_in_list(self, text_value, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		try:
			self.LOGGER.info("Read values from elements < {} > and save values in list".format(name))
			values = []
			for value in elements:
				values.append(value.text)
			self.LOGGER.info("Check occurrence_text in elements < {} >".format(name))
			if text_value in values[0]:
				pass
			else:
				BasePage.LOGGER.warning(f"Selector {name} doesn't contain expected text value\nActual{values[0]}\nExpected:{text_value}")
				raise Exception(f"Selector {name} doesn't contain expected text value\nActual{values[0]}\nExpected:{text_value}")
		except Exception as e:
			BasePage.LOGGER.warning("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't Read values from elements < {0} >. {1} \n{2}".format(name, selector, e))

	def check_text_values_of_elements(self, list,  *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		self.LOGGER.info("Read values from elements < {} > and save values in list".format(name))
		values = []
		for value in elements:
			values.append(value.text)
		try:
			assert list == values
		except AssertionError:
			BasePage.LOGGER.error("\nExpected: {0}, \nand provided: {1} lists aren't the same".format(list, values))
			raise AssertionError("\nExpected: {0}, \nand provided: {1} lists aren't the same".format(list, values))

	def save_list_of_elements(self, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		try:
			self.LOGGER.info("Save the list of < {0} > elements".format(name))
			return elements
		except Exception as e:
			BasePage.LOGGER.warning("Test can't save of list elements < {0} >. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't save of list elements < {0} >. {1} \n{2}".format(name, selector, e))

	def choice_random_element_from_list(self, *selector):
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		try:
			self.LOGGER.info("Select random element from the list of elements < {0} >".format(name))
			element = choice(elements)
			element.click()
		except Exception as e:
			BasePage.LOGGER.warning("Test can't select random element from the list < {0} > of elements. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't select random element from the list < {0} > of elements. {1} \n{2}".format(name, selector, e))


	def choice_random_element_custom_list(self, items, *selector):
		"""Random choice element from fixed number of items from a list elements
			Example:
				self.choice_random_element_custom_list(5, *self.ROTATION_LIST_BUTTONS)
				5 - first five elements
				*self.ROTATION_LIST_BUTTONS - query list of selectors
		"""
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)[:items]
		try:
			self.LOGGER.info("Select random element from the list of elements < {0} >".format(name))
			element = choice(elements)
			#self.LOGGER.info(f"Click {selector}")
			element.click()
		except Exception as e:
			BasePage.LOGGER.warning("Test can't select random element from the list < {0} > of elements. {1} \n{2}".format(name, selector, e))
			raise Exception("Test can't select random element from the list < {0} > of elements. {1} \n{2}".format(name, selector, e))


	def current_date_time_system(self):
		self.LOGGER.info("Get current date and time of OS")
		date = datetime.datetime.now()
		return date.strftime("%d.%m.%Y, %I:%M" + "%p").lower()

	def refresh_page(self):
		self.LOGGER.info("Refresh page")
		self.driver.refresh()
		
	def switch_to_iframe_then_click(self, iframe_xpath, *selector):
		iframe = self.driver.find_element(By.XPATH,  iframe_xpath)
		self.driver.switch_to_frame(iframe)
		WebDriverWait(self.driver, BasePage.timeout).until(EC.element_to_be_clickable(selector)).click()
		self.driver.switch_to.default_content()

	def switch_tab_browser(self, number_tab):
		self.LOGGER.info("Swith tab in browser")
		try:
			new_window = self.driver.window_handles[number_tab]
			self.driver.switch_to.window(new_window)
		except Exception as e:
			BasePage.LOGGER.warning(f"Test can't switch broser tab: < {e} >")
			raise Exception(f"Test can't switch broser tab: < {e} >")

	def switch_iframe(self, *selector):
		selector, name = format_selector(*selector)
		self.LOGGER.info("Switch frame {}".format(name))
		iframe = self.iframe_element_located(selector, name)
		try:
			self.LOGGER.info("Save the list of < {0} > elements".format(name))
			return iframe
		except Exception as e:
			BasePage.LOGGER.warning(
				"Test can't switch iframe < {0} > of elements. {1} \n{2}".format(name, selector, e))
			raise Exception(
				"Test can't switch iframe < {0} > of elements. {1} \n{2}".format(name, selector, e))

	def check_download_folder(self, path):
		BasePage.LOGGER.error(f"Check folder for downloads")
		try:
			if os.path.isdir(path):
				shutil.rmtree(path)
				os.mkdir(path)
			else:
				pass
		except FileNotFoundError:
			pass

	def count_downloaded_files(self, path, count=0):
		trying = 0
		found = False
		while not found or trying < 10:
			if os.path.exists(path):
				found = True
				break
			time.sleep(2)
			trying += 1
		if not found:
			raise Exception
		count_files = len(os.listdir(path))
		try:
			assert count_files == count
		except AssertionError:
			BasePage.LOGGER.error(f"The folder does not contain a certain number of files.\nExpected value: {count},\n  Actual value: {count_files}")
			raise AssertionError(f"The folder does not contain a certain number of files.\nExpected value: {count},\n  Actual value: {count_files}")
		self.check_download_folder(path)

	def click_element_by_attribute_value(self, text, *selector):
		"""

		Click on UI element in list by text value

		:param text: Text value in list
		:param selector: UI selector
		"""
		selector, name = format_selector(*selector)
		elements = self.find_elements_visibility(selector, name)
		# values = []
		try:
			values = [value for value in elements]
			for i in values:
				if i == text:
					index1 = values.index(i)
					elements[index1].click()
				else:
					pass
		except Exception as e:
			BasePage.LOGGER.warning("Test couldn't select element < {0} > by value *{1}* in the list: {2}\n{3}".format(name, text, selector, e))
			raise Exception("Test couldn't select element < {0} >  by value *{1}* in the list: {2}\n{3}".format(name, text, selector, e))


	def select_value_in_dropdown_by_option_visible_text(self, text, *selector):
		selector, name = format_selector(*selector)
		element = Select (self.find_element_located(selector, name))
		self.LOGGER.info(f"Select value < {text} > from the list *{selector}*")
		try:
			element.select_by_visible_text(text)
		except Exception as e:
			BasePage.LOGGER.warning(f"Test couldn't select value by text: < {text} > in the list: {selector}")
			raise Exception(f"Test couldn't select value by text: < {text} >  in the list: {selector}")

	def edit_text_field(self, key_send, *selector):
		selector, name = format_selector(*selector)
		element = self.find_element_clickable(selector, name)
		self.LOGGER.info("Input text value:* {} * to < {} > element.".format(key_send, name))
		try:
			self.backspace_button_all(*selector)
			element.send_keys(key_send)
		except Exception as e:
			BasePage.LOGGER.warning("Test input text to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))
			raise Exception("Test can't input text to element < {0} >\n < {1} > \n < {2} >".format(name, selector, e))

	def list_selectors_doesnt_contain_value(self, text, *selector):
		selector, name = format_selector(*selector)
		list_values = [self.read_values_from_elements_and_make_list_text_values(*selector)]
		for i in list_values:
			if text != i:
				continue
			else:
				BasePage.LOGGER.warning(f"List of selectors {name} still contain value: {text}")
				raise AssertionError(f"List of selectors {name} still contain value: {text}")
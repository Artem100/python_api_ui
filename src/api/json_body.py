class JSONBody:

    @staticmethod
    def some_json_body(value1, value2):
        json = {
            "key1": value1,
            "key2": value2
        }

        return json